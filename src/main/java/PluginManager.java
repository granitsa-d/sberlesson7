import java.io.File;
import java.net.*;

public class PluginManager {
    private final String pluginRootDirectory;

    public PluginManager(String pluginRootDirectory) {this.pluginRootDirectory = pluginRootDirectory;}

    public Plugin load(String pluginName, String pluginClassName) throws MalformedURLException, ClassNotFoundException, IllegalAccessException, InstantiationException, URISyntaxException {

        String jar = pluginRootDirectory + "\\" + pluginName + ".jar";

        File file = new File(jar);
        URL url = file.toURI().toURL();

        URLClassLoader classLoader = new URLClassLoader(new URL[]{url});
        Class pluginClass = classLoader.loadClass(pluginClassName);
        Plugin instance = (Plugin) pluginClass.newInstance();
        return instance;
    }
}
