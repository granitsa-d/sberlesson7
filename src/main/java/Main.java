import java.net.MalformedURLException;
import java.net.URISyntaxException;

public class Main {
    public static void main(String[] args) throws ClassNotFoundException, MalformedURLException, InstantiationException, IllegalAccessException, URISyntaxException {
        PluginManager pluginManager = new PluginManager("D:\\Java\\sberLesson7\\plugins");

        Plugin[] plugins = new Plugin[2];
        plugins[0] = pluginManager.load("plugImpl1-1.0-SNAPSHOT", "PluginImpl");
        plugins[1] = pluginManager.load("plugImpl2-1.0-SNAPSHOT", "PluginImpl");

        for (Plugin plugin: plugins) {
            plugin.doUsefull();
        }
    }
}
